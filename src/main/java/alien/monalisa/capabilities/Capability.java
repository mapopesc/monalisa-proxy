package alien.monalisa.capabilities;

import java.util.Properties;

public interface Capability extends Runnable {
    String getCapabilityName();
    void loadProperties(Properties prop);
    default String getPropertyOrThrowError(Properties prop, String propKey) {
        final String res = prop.getProperty(propKey);
        if (res == null) {
            throw new IllegalStateException(String.format("Key %s not found in properties", propKey));
        } else {
            return res;
        }
    }
}
