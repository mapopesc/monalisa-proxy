package alien.monalisa.capabilities.farm.utils;

import lia.Monitor.monitor.Result;

import java.util.regex.Pattern;

// TODO: this should match on more complex conditions: farm details, module details, param details, etc
public class SubscribePatternMatcher {
    final Pattern conditionsPattern;

    public SubscribePatternMatcher(String pattern) {
        conditionsPattern = Pattern.compile(pattern);
    }

    public boolean patternMatches(Result result) {
        return conditionsPattern.matcher(result.ClusterName).matches();
    }
}
