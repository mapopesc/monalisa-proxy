package alien.monalisa.capabilities.farm.utils;

import alien.monalisa.FilterBus;
import alien.monalisa.capabilities.farm.FarmCapability;
import alien.monalisa.examples.cristi.RandomDataMonitoringModule;
import lia.Monitor.modules.monProcStat;
import lia.Monitor.monitor.MNode;
import lia.Monitor.monitor.Result;

import java.net.InetAddress;
import java.util.List;
import java.util.logging.Logger;

import static java.util.Collections.emptyList;

public class ModuleStarters {
    private static Logger logger = Logger.getLogger(ModuleStarters.class.getName());
    private final FilterBus filterBus = new FilterBus("MARKFARM");
    public ModuleStarters() {
    }

    public void runRandomModule() {
        final RandomDataMonitoringModule module = new RandomDataMonitoringModule();
        module.init(new MNode("localhost", "127.0.0.1", null, null), Integer.toString(10));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        logger.info("Started thread that offers objects to bus");
        while (true) {
            Result res = null;
            try {
                res = module.doProcess();
                if (res != null) {
                    filterBus.addNewResult(res);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void runProcModule() {
        // test MonALISA module
        String host = "localhost";
        try {
            final monProcStat mon = new monProcStat();

            String address = null;

            try {
                address = InetAddress.getByName(host).getHostAddress();
            } catch (Exception var6) {
                System.out.println(" Can not get ip for node " + var6);
                System.exit(-1);
            }

            mon.init(new MNode(host, address, null, null), null, null);

            try {
                while (true) {
                    Result res = (Result) mon.doProcess();
                    if (res != null) {
                        filterBus.addNewResult(res);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
