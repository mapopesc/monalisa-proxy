package alien.monalisa.capabilities.farm;

import alien.monalisa.MonALISANode;
import alien.monalisa.capabilities.Capability;
import alien.monalisa.capabilities.farm.utils.ModuleStarters;
import alien.monalisa.messages.InitialRegistrationMessage;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static alien.monalisa.capabilities.farm.FarmKeyProperties.*;

public class FarmCapability implements Capability {
    private final static Logger logger = Logger.getLogger(FarmCapability.class.getName());
    private URI proxyEndpoint;
    private MonALISANode thisNode;
    public FarmWsClient sender;

    public FarmCapability(Properties prop) {
        loadProperties(prop);
    }

    @Override
    public String getCapabilityName() {
        return "Farm";
    }

    @Override
    public void loadProperties(Properties prop) {
        final String proxyUrlValue = getPropertyOrThrowError(prop, PROXY_ENDPOINT.getPropertyKey());
        final String serviceName = getPropertyOrThrowError(prop, FARM_NAME.getPropertyKey());
        final String serviceIp = getPropertyOrThrowError(prop, FARM_IP.getPropertyKey());
        final String serviceHostname = getPropertyOrThrowError(prop, FARM_HOSTNAME.getPropertyKey());

        this.thisNode = new MonALISANode(serviceName, serviceIp, serviceHostname, Set.of(this.getCapabilityName()));

        try {
            proxyEndpoint = new URI(proxyUrlValue);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        this.sender = new FarmWsClient(proxyEndpoint, thisNode.serviceName);
        final InitialRegistrationMessage initialMessage = new InitialRegistrationMessage(thisNode);
        // TODO: this is a pretty basic retry mechanism, something more sophisticated is needed
        while (!sender.sendMessage(initialMessage)) {
            logger.warning("Could not register this node to the proxy, retrying after 1s...");
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            sender.sendMessage(initialMessage);
        }
    }

    // THE BELOW CODE IS FOR TESTING PURPOSES ONLY, SHOULD BE REMOVED ON A LATER ITERATION
    public static void main(String[] args) throws IOException {
        // farm config file setup
        String farmConfigFile = System.getProperty("FARM_CONFIG_FILE");
        if (farmConfigFile == null) {
            farmConfigFile = "src/main/resources/default-farm.properties";
        }

        // farm setup
        final Properties testingFarmProperties = new Properties();
        testingFarmProperties.load(new FileInputStream(farmConfigFile));
        final FarmCapability farm = new FarmCapability(testingFarmProperties);
        farm.run();

        // modules setup
        final ModuleStarters moduleStarters = new ModuleStarters();
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(moduleStarters::runRandomModule);
    }
}



