package alien.monalisa.capabilities.farm;

import alien.monalisa.FilterBus;
import alien.monalisa.Pipeline;
import alien.monalisa.capabilities.FarmUtils;
import alien.monalisa.messages.CancelSubscriptionMessage;
import alien.monalisa.messages.DataPipelineMessage;
import alien.monalisa.messages.SubscribeRequestMessage;
import alien.monalisa.websocket.GenericObjectDecoder;
import alien.monalisa.websocket.GenericObjectEncoder;
import jakarta.websocket.*;
import lia.Monitor.monitor.Result;
import lia.Monitor.monitor.eResult;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.regex.Pattern;

@ClientEndpoint(encoders = {GenericObjectEncoder.class}, decoders = {GenericObjectDecoder.class})
public class FarmWsClient {
    private final static Logger logger = Logger.getLogger(FarmWsClient.class.getName());
    private final String farmServiceName;

    private Session session = null;
    private final static Map<String, Pipeline> activePipelines = new ConcurrentHashMap<>();

    public FarmWsClient(URI proxyEndpoint, String farmServiceName) {
        this.farmServiceName = farmServiceName;
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, proxyEndpoint);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @OnOpen
    public void farmOnOpen(Session session) {
        this.session = session;
    }

    @OnMessage
    public void farmOnMessage(Object object, Session session) {
        if (object instanceof SubscribeRequestMessage) {
            final SubscribeRequestMessage message = (SubscribeRequestMessage) object;
            if (subscribePatternMatches(message.getFarmSubscribePattern())) {
                final Pipeline p = new Pipeline(message, message.getRequestNode());
                activePipelines.put(message.getPipelineId(), p);
                final FarmUtils utils = new FarmUtils(); // TODO: IS THIS REALLY NECESSARY
                FilterBus.addNewBus(utils.createPipelineBus(p, this));
                logger.info("Successfully registered pipeline " + message.getPipelineId());
            } else {
                logger.info("Pipeline " + message.getPipelineId() + " ignored");
            }
        } else if (object instanceof CancelSubscriptionMessage) {
            final CancelSubscriptionMessage message = (CancelSubscriptionMessage) object;
            final Pipeline pipeline = activePipelines.remove(message.getPipelineId());
            if (pipeline == null) {
                logger.warning("Got a cancel subscription request for pipeline " + message.getPipelineId() + " that is not registered");
            } else {
                logger.info("Successfully canceled pipeline " + message.getPipelineId());
            }
        }
    }

    public boolean sendMessage(Object object) {
        if (session != null && session.isOpen()) {
            try {
                this.session.getAsyncRemote().sendObject(object, (r) -> {});
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            return false;
        }
    }

    public void sendDataPipelineMessage(List<Result> results, List<eResult> eResults) {
        for (Pipeline pipeline : activePipelines.values()) {
            DataPipelineMessage pipelineMessage = new DataPipelineMessage(pipeline.getPipelineId(), results, eResults);
            sendMessage(pipelineMessage);
        }
    }

    private boolean subscribePatternMatches(String pattern) {
        return Pattern.matches(pattern, farmServiceName);
    }
}
