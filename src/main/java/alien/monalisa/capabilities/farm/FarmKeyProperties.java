package alien.monalisa.capabilities.farm;

public enum FarmKeyProperties {
    PROXY_ENDPOINT("capability.farm.proxy_url"),
    FARM_NAME("capability.farm.name"),
    FARM_IP("capability.farm.ip"),
    FARM_HOSTNAME("capability.farm.hostname");
    private final String propertyKey;
    FarmKeyProperties(String propertyKey) {
        this.propertyKey = propertyKey;
    }
    public String getPropertyKey() {
        return propertyKey;
    }
}
