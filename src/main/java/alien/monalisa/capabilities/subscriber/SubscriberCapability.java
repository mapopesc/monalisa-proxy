package alien.monalisa.capabilities.subscriber;

import alien.monalisa.MonALISANode;
import alien.monalisa.capabilities.Capability;
import alien.monalisa.messages.InitialRegistrationMessage;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import static alien.monalisa.capabilities.subscriber.SubscriberKeyProperties.*;

public class SubscriberCapability implements Capability {
    private final static Logger logger = Logger.getLogger(SubscriberCapability.class.getName());
    private URI proxyEndpoint;
    private MonALISANode thisNode;

    public SubscriberWsClient receiver;

    public SubscriberCapability(Properties prop) {
        loadProperties(prop);
    }

    @Override
    public String getCapabilityName() {
        return "Subscriber";
    }

    @Override
    public void loadProperties(Properties prop) {
        final String proxyUrlValue = getPropertyOrThrowError(prop, PROXY_ENDPOINT.getPropertyKey());
        final String serviceName = getPropertyOrThrowError(prop, SUBSCRIBER_NAME.getPropertyKey());
        final String serviceIp = getPropertyOrThrowError(prop, SUBSCRIBER_IP.getPropertyKey());
        final String serviceHostname = getPropertyOrThrowError(prop, SUBSCRIBER_HOSTNAME.getPropertyKey());

        this.thisNode = new MonALISANode(serviceName, serviceIp, serviceHostname, Set.of(this.getCapabilityName()));

        try {
            proxyEndpoint = new URI(proxyUrlValue);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        this.receiver = new SubscriberWsClient(proxyEndpoint, thisNode.serviceName);
        final InitialRegistrationMessage initialMessage = new InitialRegistrationMessage(thisNode);
        // TODO: this is a pretty basic retry mechanism, something more sophisticated is needed
        while (!receiver.sendMessage(initialMessage)) {
            logger.warning("Could not register this node to the proxy, retrying after 1s...");
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            receiver.sendMessage(initialMessage);
        }
    }

    // THE BELOW CODE IS FOR TESTING PURPOSES ONLY, SHOULD BE REMOVED ON A LATER ITERATION
    public static void main(String[] args) throws InterruptedException, IOException {
        // subscriber config file setup
        String subscriberConfigFile = System.getProperty("SUBSCRIBER_CONFIG_FILE");
        if (subscriberConfigFile == null) {
            subscriberConfigFile = "src/main/resources/default-subscriber.properties";
        }

        // subscriber setup
        final Properties testingSubscriberProperties = new Properties();
        testingSubscriberProperties.load(new FileInputStream(subscriberConfigFile));
        SubscriberCapability subscriber = new SubscriberCapability(testingSubscriberProperties);
        subscriber.run();

        // run setup
        final String farmPattern = testingSubscriberProperties.getProperty(SUBSCRIBER_FARM_PATTERN.getPropertyKey(), ".*");
        final String modulePattern = testingSubscriberProperties.getProperty(SUBSCRIBER_MODULE_PATTERN.getPropertyKey(), ".*");

        subscriber.receiver.registerNewPipeline((m) -> {}, farmPattern, modulePattern, subscriber.thisNode);
        Thread.sleep(10000000L);
    }
}
