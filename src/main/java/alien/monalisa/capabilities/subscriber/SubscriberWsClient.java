package alien.monalisa.capabilities.subscriber;

import alien.monalisa.MonALISANode;
import alien.monalisa.messages.DataPipelineMessage;
import alien.monalisa.messages.SubscribeRequestMessage;
import alien.monalisa.websocket.GenericObjectDecoder;
import alien.monalisa.websocket.GenericObjectEncoder;
import jakarta.websocket.*;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;

import static java.util.UUID.randomUUID;

@ClientEndpoint(encoders = {GenericObjectEncoder.class}, decoders = {GenericObjectDecoder.class})
public class SubscriberWsClient {
    private final static Logger logger = Logger.getLogger(SubscriberWsClient.class.getName());
    private final String subscriberServiceName;
    private Session session = null;

    private final static Map<String, Consumer<DataPipelineMessage>> activePipelines = new HashMap<>();

    public SubscriberWsClient(URI proxyEndpoint, String subscriberServiceName) {
        this.subscriberServiceName = subscriberServiceName;
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, proxyEndpoint);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @OnOpen
    public void subscriberOnOpen(Session session) {
        this.session = session;
    }

    @OnMessage
    public void subscriberOnMessage(Object object, Session session) {
        if (object instanceof DataPipelineMessage) {
            final DataPipelineMessage message = (DataPipelineMessage) object;
            Consumer<DataPipelineMessage> consumer = activePipelines.get(message.getPipelineId());
            if (consumer == null) {
                logger.warning("Got a data message for pipeline " + message.getPipelineId() + " which is not internally registered");
            } else {
                consumer.accept(message);
            }
        }
    }

    public boolean sendMessage(Object object) {
        if (session != null && session.isOpen()) {
            try {
                this.session.getBasicRemote().sendObject(object);
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            return false;
        }
    }

    public boolean registerNewPipeline(Consumer<DataPipelineMessage> consumer, String farmPattern,
                                       String modulePattern, MonALISANode requestNode) {
        final String pipelineId = randomUUID().toString();
        final SubscribeRequestMessage message = new SubscribeRequestMessage(pipelineId, farmPattern, modulePattern, requestNode);
        activePipelines.put(pipelineId, consumer);
        boolean messageSent = sendMessage(message);
        if (!messageSent) {
            activePipelines.remove(pipelineId);
        }
        return messageSent;
    }
}
