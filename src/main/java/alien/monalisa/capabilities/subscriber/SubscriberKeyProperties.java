package alien.monalisa.capabilities.subscriber;

public enum SubscriberKeyProperties {
    PROXY_ENDPOINT("capability.subscriber.proxy_url"),
    SUBSCRIBER_NAME("capability.subscriber.name"),
    SUBSCRIBER_IP("capability.subscriber.ip"),
    SUBSCRIBER_HOSTNAME("capability.subscriber.hostname"),
    SUBSCRIBER_FARM_PATTERN("capability.subscriber.farm_pattern"),
    SUBSCRIBER_MODULE_PATTERN("capability.subscriber.module_pattern");
    private final String propertyKey;
    SubscriberKeyProperties(String propertyKey) {
        this.propertyKey = propertyKey;
    }
    public String getPropertyKey() {
        return propertyKey;
    }
}
