package alien.monalisa.capabilities;

import alien.monalisa.Pipeline;
import alien.monalisa.capabilities.farm.FarmWsClient;
import alien.monalisa.shapes.Bus;
import alien.monalisa.shapes.BusShape;
import alien.monalisa.shapes.FilterShape;
import alien.monalisa.shapes.OutputShape;
import lia.Monitor.monitor.Result;

import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import static alien.monalisa.shapes.BusUtils.getAndStartSingleThreadedBusFromShape;
import static java.util.Collections.emptyList;

public class FarmUtils {
    public Bus<Result, Result> createPipelineBus(Pipeline pipeline, FarmWsClient wsClient) {
        BusShape<Result, Result> resShape = new BusShape<>();
        resShape = resShape.via(new PipelineFilter(pipeline.getFarmSubscribePattern(), pipeline.getModuleSubscribesPattern()));
        // TODO: THIS NEEDS TO BE MADE MORE EFFICIENT, USING COLLECTIONS IN ALL OVER THE PLACE
        //       IT FEELS LIKE WE CONVERT LISTS TO INDIVIDUAL ITEMS TO LISTS ALL OVER AGAIN
        resShape = resShape.to(new PipelineOutput((data) -> wsClient.sendDataPipelineMessage(List.of(data), emptyList())));

        return getAndStartSingleThreadedBusFromShape(resShape, 100, "pipeline-bus-" + pipeline.getPipelineId());
    }

    class PipelineFilter implements FilterShape<Result> {
        private final Pattern farmMatcher;
        private final Pattern moduleMatcher;

        PipelineFilter(String farmPattern, String modulePattern) {
            farmMatcher = Pattern.compile(farmPattern);
            moduleMatcher = Pattern.compile(modulePattern);
        }

        @Override
        public Result filter(Result data) {
            if (farmMatcher.matcher(data.FarmName).matches() && moduleMatcher.matcher(data.Module).matches()) {
                return data;
            } else {
                return null;
            }
        }
    }

    class PipelineOutput implements OutputShape<Result> {
        private final Consumer<Result> resultConsumer;

        public PipelineOutput(Consumer<Result> resultConsumer) {
            this.resultConsumer = resultConsumer;
        }

        @Override
        public Result write(Result data) {
            resultConsumer.accept(data);
            return data;
        }
    }
}
