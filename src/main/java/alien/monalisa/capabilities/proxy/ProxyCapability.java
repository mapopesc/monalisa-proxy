package alien.monalisa.capabilities.proxy;

import alien.monalisa.capabilities.Capability;
import jakarta.servlet.ServletContextEvent;
import jakarta.websocket.DeploymentException;
import jakarta.websocket.server.ServerContainer;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.servlets.DefaultServlet;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.websocket.server.Constants;
import org.apache.tomcat.websocket.server.WsContextListener;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static alien.monalisa.capabilities.proxy.ProxyKeyProperties.*;
import static java.lang.Integer.parseInt;

public class ProxyCapability implements Capability {
    private Connector connector;
    private Integer wsPort;

    public ProxyCapability(Properties prop) {
        loadProperties(prop);
    }
    @Override
    public String getCapabilityName() {
        return "Proxy";
    }

    @Override
    public void loadProperties(Properties prop) {
        this.wsPort = parseInt(getPropertyOrThrowError(prop, PROXY_WS_PORT.getPropertyKey()));

        connector = new Connector("org.apache.coyote.http11.Http11Nio2Protocol");
        connector.setProperty("address", getPropertyOrThrowError(prop, PROXY_WS_BINDING_ADDRESS.getPropertyKey()));
        connector.setPort(this.wsPort);
        connector.setProperty("maxThreads", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_MAX_THREADS.getPropertyKey()));
        connector.setProperty("maxConnections", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_MAX_CONNECTIONS.getPropertyKey()));
        connector.setProperty("connectionTimeout", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_CONNECTION_TIMEOUT.getPropertyKey()));
        connector.setProperty("compression", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_COMPRESSION.getPropertyKey()));
        connector.setProperty("compressionMinSize", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_COMPRESSION_MIN_SIZE.getPropertyKey()));
        connector.setProperty("useSendFile", getPropertyOrThrowError(prop, PROXY_WS_CONNECTOR_USE_SEND_FILE.getPropertyKey()));
    }

    @Override
    public void run() {
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir(System.getProperty("java.io.tmpdir"));
        tomcat.setPort(this.wsPort);

        tomcat.getService().removeConnector(tomcat.getConnector()); // remove default connector

        tomcat.setConnector(this.connector);

        // Add an empty Tomcat context
        final Context ctx = tomcat.addContext("", null);

        // Configure websocket context listener
        ctx.addApplicationListener(Config.class.getName());
        Tomcat.addServlet(ctx, "default", new DefaultServlet());
        ctx.addServletMappingDecoded("/", "default");

        try {
            tomcat.start();

            new Thread(() -> tomcat.getServer().await()).start();
        } catch (LifecycleException e) {
            throw new RuntimeException(e);
        }
    }

    public static class Config extends WsContextListener {
        @Override
        public void contextInitialized(ServletContextEvent sce) {
            super.contextInitialized(sce);
            ServerContainer sc = (ServerContainer) sce.getServletContext()
                    .getAttribute(Constants.SERVER_CONTAINER_SERVLET_CONTEXT_ATTRIBUTE);
            try {
                sc.addEndpoint(ProxyWsServer.class);
            } catch (DeploymentException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        // proxy config file setup
        String proxyConfigFile = System.getProperty("PROXY_CONFIG_FILE");
        if (proxyConfigFile == null) {
            proxyConfigFile = "src/main/resources/default-proxy.properties";
        }

        // proxy setup
        final Properties testingProxyProperties = new Properties();
        testingProxyProperties.load(new FileInputStream(proxyConfigFile));
        ProxyCapability proxyCapability = new ProxyCapability(testingProxyProperties);
        proxyCapability.run();
    }
}
