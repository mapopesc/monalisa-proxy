package alien.monalisa.capabilities.proxy;

import alien.monalisa.messages.WsMessage;
import alien.monalisa.shapes.Bus;
import alien.monalisa.websocket.GenericObjectDecoder;
import alien.monalisa.websocket.GenericObjectEncoder;
import jakarta.websocket.*;
import jakarta.websocket.server.ServerEndpoint;

import java.io.IOException;
import java.util.logging.Logger;

import static alien.monalisa.capabilities.proxy.EncapsulatedWsMessage.SessionAction.ON_CLOSE;
import static alien.monalisa.capabilities.proxy.EncapsulatedWsMessage.SessionAction.ON_MESSAGE;
import static alien.monalisa.shapes.BusUtils.getAndStartSingleThreadedBusFromOutput;

@ServerEndpoint(value = "/proxy", encoders = {GenericObjectEncoder.class}, decoders = {GenericObjectDecoder.class})
public class ProxyWsServer {
    private final static Logger logger = Logger.getLogger(ProxyWsServer.class.getName());
    private final static int COMMAND_CENTER_BUS_QUEUE_SIZE = 1000;
    private final static Bus<EncapsulatedWsMessage, EncapsulatedWsMessage> wsMessageBus =
            getAndStartSingleThreadedBusFromOutput(new ProxyCommandCenter(), COMMAND_CENTER_BUS_QUEUE_SIZE, "wsInputBus");

    @OnOpen
    public void proxyOnOpen(Session session) {
        // TODO: definitely something should be done here, don't know what yet
    }

    @OnMessage
    public void proxyOnMessage(Object object, Session session) throws EncodeException, IOException {
        if (object instanceof WsMessage) {
            wsMessageBus.offer(new EncapsulatedWsMessage((WsMessage) object, session, ON_MESSAGE));
        }
    }

    @OnClose
    public void proxyOnClose(Session session, CloseReason reason) throws EncodeException, IOException {
        wsMessageBus.offer(new EncapsulatedWsMessage(null, session, ON_CLOSE));
    }
}
