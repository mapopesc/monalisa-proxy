package alien.monalisa.capabilities.proxy;

import jakarta.websocket.Session;

import java.util.List;

public class EncapsulatedWsResponse {
    private final List<Session> sessionList;
    private final Object message;

    public EncapsulatedWsResponse(Session session, Object message) {
        this(List.of(session), message);
    }
    public EncapsulatedWsResponse(List<Session> sessionList, Object message) {
        this.sessionList = sessionList;
        this.message = message;
    }

    public List<Session> getSessionList() {
        return sessionList;
    }

    public Object getMessage() {
        return message;
    }
}
