package alien.monalisa.capabilities.proxy;

import alien.monalisa.MonALISANode;
import alien.monalisa.Pipeline;
import alien.monalisa.messages.*;
import alien.monalisa.shapes.Bus;
import alien.monalisa.shapes.OutputShape;
import jakarta.websocket.Session;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static alien.monalisa.capabilities.proxy.EncapsulatedWsMessage.SessionAction.ON_CLOSE;
import static alien.monalisa.capabilities.proxy.EncapsulatedWsMessage.SessionAction.ON_MESSAGE;
import static alien.monalisa.shapes.BusUtils.getAndStartSingleThreadedBusFromOutput;

public class ProxyCommandCenter implements OutputShape<EncapsulatedWsMessage> {
    private final static Logger logger = Logger.getLogger(ProxyCommandCenter.class.getName());
    private final static int OUTPUT_BUS_QUEUE_SIZE = 1000;
    // Unfortunately Java SDK does not have bidirectional maps
    private final Map<Session, MonALISANode> currentRegisteredNodes = new HashMap<>();
    private final Map<MonALISANode, Session> currentRegisteredSessions = new HashMap<>();

    private final Map<String, Pipeline> activePipelines = new HashMap<>();
    private final ExecutorService outputBusThreadPool = Executors.newFixedThreadPool(1);
    private final Bus<EncapsulatedWsResponse, EncapsulatedWsResponse> wsResponseBus;

    public ProxyCommandCenter() {
        this.wsResponseBus = getAndStartSingleThreadedBusFromOutput(new WsOutput(), OUTPUT_BUS_QUEUE_SIZE, "wsOutputBus");
    }

    @Override
    public EncapsulatedWsMessage write(EncapsulatedWsMessage data) {
        Session session = data.getWsSession();

        if (data.getSessionAction().equals(ON_MESSAGE)) {
            WsMessage object = data.getMessage();

            if (object instanceof InitialRegistrationMessage) {
                final MonALISANode node = ((InitialRegistrationMessage) object).getNode();
                currentRegisteredNodes.put(session, node);
                currentRegisteredSessions.put(node, session);
                logger.info("Registered node " + node.serviceName + " into the currently registered sessions");


                if (node.capabilities.contains("Farm")) {
                    for (Pipeline pipeline : activePipelines.values()) {
                        final SubscribeRequestMessage newMessage = new SubscribeRequestMessage(pipeline);
                        wsResponseBus.offer(new EncapsulatedWsResponse(session, newMessage));
                    }
                    logger.info("Informed farm " + node.serviceName + " about all the active subscriptions");
                }
            } else if (object instanceof SubscribeRequestMessage) {
                final SubscribeRequestMessage message = (SubscribeRequestMessage) object;

                final MonALISANode node = currentRegisteredNodes.get(session);
                if (node == null) {
                    throw new IllegalStateException("Subscribe message from unregistered connection");
                }
                logger.info("Got a subscription request message from " + node.serviceName + " for pipeline " + message.getPipelineId());

                Pipeline newPipeline = new Pipeline(message, node);
                activePipelines.put(newPipeline.getPipelineId(), newPipeline);

                logger.info("Informing all farms about the pipeline " + message.getPipelineId());
                for (Map.Entry<Session, MonALISANode> entry : currentRegisteredNodes.entrySet()) {
                    if (entry.getValue().capabilities.contains("Farm")) {
                        wsResponseBus.offer(new EncapsulatedWsResponse(entry.getKey(), message));
                    }
                }
                logger.info("All farms informed successfully about pipeline " + message.getPipelineId());
            } else if (object instanceof CancelSubscriptionMessage) {
                final CancelSubscriptionMessage message = (CancelSubscriptionMessage) object;

                final MonALISANode node = currentRegisteredNodes.get(session);
                if (node == null) {
                    throw new IllegalStateException("Subscribe cancellation message from unregistered connection");
                }
                logger.info("Got a subscription cancellation message from " + node.serviceName + " for pipeline " + message.getPipelineId());

                Pipeline pipeline = activePipelines.get(message.getPipelineId());
                if (pipeline == null) {
                    throw new IllegalStateException("Pipeline " + message.getPipelineId() + " is not registered");
                }

                informProducersOfPipelineCancellation(pipeline);

                activePipelines.remove(message.getPipelineId());

                logger.info("Successfully stopped pipeline " + pipeline.getPipelineId());
            } else if (object instanceof DataPipelineMessage) {
                final DataPipelineMessage message = (DataPipelineMessage) object;
                final Pipeline pipeline = activePipelines.get(message.getPipelineId());
                if (pipeline == null) {
                    logger.warning("Got a DataMessage for unregistered pipeline " + message.getPipelineId());
                    throw new IllegalStateException();
                }

                Session consumerSession = currentRegisteredSessions.get(pipeline.getConsumer());
                wsResponseBus.offer(new EncapsulatedWsResponse(consumerSession, message));
            }
        } else if (data.getSessionAction().equals(ON_CLOSE)) {
            final MonALISANode node = currentRegisteredNodes.remove(session);
            if (node != null) {
                logger.info("Processing connection closed for node " + node.serviceName);

                currentRegisteredSessions.remove(node);
                List<Pipeline> consumedPipelines = activePipelines.values().stream()
                        .filter(pipeline -> pipeline.getConsumer().equals(node))
                        .collect(Collectors.toList());
                List<Pipeline> producedPipelines = activePipelines.values().stream()
                        .filter(pipeline -> pipeline.getProducers().contains(node))
                        .collect(Collectors.toList());

                logger.info(node.serviceName + "had " + producedPipelines.size() + " active pipelines as a producer");
                producedPipelines.forEach(pipeline -> pipeline.removeProducer(node));

                logger.info(node.serviceName + " had " + consumedPipelines.size() + " active pipelines as a consumer");
                for (final Pipeline pipeline : consumedPipelines) {
                    informProducersOfPipelineCancellation(pipeline);
                }

                logger.info("Node " + node.serviceName + " deregistered successfully");
            } else {
                logger.warning("Connection closed from unregistered node");
            }
        }

        return null;
    }

    private void informProducersOfPipelineCancellation(Pipeline pipeline) {
        logger.info("Informing producers of subscribe cancellation request for pipeline " + pipeline.getPipelineId());
        List<Session> producerSessions = pipeline.getProducers().stream()
                .map(currentRegisteredSessions::get)
                .collect(Collectors.toList());

        for (final Session s : producerSessions) {
            wsResponseBus.offer(new EncapsulatedWsResponse(s, new CancelSubscriptionMessage(pipeline.getPipelineId())));
        }

        logger.info("All messages related to pipeline " + pipeline.getPipelineId() + " have been sent");
    }
}
