package alien.monalisa.capabilities.proxy;

import alien.monalisa.messages.WsMessage;
import jakarta.websocket.Session;

public class EncapsulatedWsMessage {
    private final WsMessage message;
    private final Session wsSession;
    private final SessionAction sessionAction;

    public EncapsulatedWsMessage(WsMessage message, Session wsSession, SessionAction sessionAction) {
        this.message = message;
        this.wsSession = wsSession;
        this.sessionAction = sessionAction;
    }

    public WsMessage getMessage() {
        return message;
    }

    public Session getWsSession() {
        return wsSession;
    }

    public SessionAction getSessionAction() {
        return sessionAction;
    }

    public enum SessionAction {
        ON_OPEN,
        ON_MESSAGE,
        ON_CLOSE
    }
}
