package alien.monalisa.capabilities.proxy;

import alien.monalisa.shapes.OutputShape;
import jakarta.websocket.Session;

public class WsOutput implements OutputShape<EncapsulatedWsResponse> {
    @Override
    public EncapsulatedWsResponse write(EncapsulatedWsResponse data) {
        for (Session session : data.getSessionList()) {
            session.getAsyncRemote().sendObject(data.getMessage());
        }
        return null;
    }
}
