package alien.monalisa.capabilities.proxy;

public enum ProxyKeyProperties {
    PROXY_WS_PORT("capability.proxy.ws.port"),
    PROXY_WS_BINDING_ADDRESS("capability.proxy.ws.address"),
    PROXY_WS_CONNECTOR_MAX_THREADS("capability.proxy.ws.maxThreads"),
    PROXY_WS_CONNECTOR_MAX_CONNECTIONS("capability.proxy.ws.maxConnections"),
    PROXY_WS_CONNECTOR_CONNECTION_TIMEOUT("capability.proxy.ws.connectionTimeout"),
    PROXY_WS_CONNECTOR_COMPRESSION("capability.proxy.ws.compression"),
    PROXY_WS_CONNECTOR_COMPRESSION_MIN_SIZE("capability.proxy.ws.compressionMinSize"),
    PROXY_WS_CONNECTOR_USE_SEND_FILE("capability.proxy.ws.useSendFile");
    private final String propertyKey;
    ProxyKeyProperties(String propertyKey) {
        this.propertyKey = propertyKey;
    }
    public String getPropertyKey() {
        return propertyKey;
    }
}
