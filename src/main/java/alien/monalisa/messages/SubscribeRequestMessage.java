package alien.monalisa.messages;

import alien.monalisa.MonALISANode;
import alien.monalisa.Pipeline;

import java.io.Serializable;

public class SubscribeRequestMessage extends WsMessage implements Serializable {
    private static final long serialVersionUID = -8762271197499339235L;
    private final String pipelineId;
    private final String farmSubscribePattern;
    private final String moduleSubscribePattern;
    private final MonALISANode requestNode;

    // TODO: THIS NEEDS TO BE A COMPLEX OBJECT, TO CLEARLY DELIMIT VARIOUS FARM AND MODULE PROPERTIES
    public SubscribeRequestMessage(String pipelineId, String farmSubscribePattern, String moduleSubscribePattern, MonALISANode requestNode) {
        this.pipelineId = pipelineId;
        this.farmSubscribePattern = farmSubscribePattern;
        this.moduleSubscribePattern = moduleSubscribePattern;
        this.requestNode = requestNode;
    }

    public SubscribeRequestMessage(Pipeline pipeline) {
        this.pipelineId = pipeline.getPipelineId();
        this.farmSubscribePattern = pipeline.getFarmSubscribePattern();
        this.moduleSubscribePattern = pipeline.getModuleSubscribesPattern();
        this.requestNode = pipeline.getConsumer();
    }

    public String getPipelineId() {
        return pipelineId;
    }

    public String getFarmSubscribePattern() {
        return farmSubscribePattern;
    }

    public String getModuleSubscribePattern() {
        return moduleSubscribePattern;
    }

    public MonALISANode getRequestNode() {
        return requestNode;
    }
}
