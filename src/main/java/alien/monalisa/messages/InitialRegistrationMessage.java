package alien.monalisa.messages;

import alien.monalisa.MonALISANode;

import java.io.Serializable;

public class InitialRegistrationMessage extends WsMessage implements Serializable {
    private static final long serialVersionUID = 4731952979198308213L;
    public final MonALISANode node;

    public InitialRegistrationMessage(MonALISANode node) {
        this.node = node;
    }

    public MonALISANode getNode() {
        return node;
    }
}
