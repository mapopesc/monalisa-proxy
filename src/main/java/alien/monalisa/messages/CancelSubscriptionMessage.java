package alien.monalisa.messages;

import java.io.Serializable;

public class CancelSubscriptionMessage extends WsMessage implements Serializable {
    private static final long serialVersionUID = 2028194579856773443L;
    private final String pipelineId;

    public CancelSubscriptionMessage(String pipelineId) {
        this.pipelineId = pipelineId;
    }

    public String getPipelineId() {
        return pipelineId;
    }
}
