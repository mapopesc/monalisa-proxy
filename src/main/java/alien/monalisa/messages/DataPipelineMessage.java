package alien.monalisa.messages;

import lia.Monitor.monitor.Result;
import lia.Monitor.monitor.eResult;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.unmodifiableList;

public class DataPipelineMessage extends WsMessage implements Serializable {
    private static final long serialVersionUID = 3790165301954246889L;
    public final String pipelineId;
    public final List<Result> results;
    public final List<eResult> eResults;

    public DataPipelineMessage(String pipelineId, List<Result> results, List<eResult> eResults) {
        this.pipelineId = pipelineId;
        this.results = results;
        this.eResults = eResults;
    }

    public String getPipelineId() {
        return pipelineId;
    }

    public List<Result> getResults() {
        return unmodifiableList(results);
    }

    public List<eResult> geteResults() {
        return unmodifiableList(eResults);
    }
    
    @Override
    public String toString() {
        String resultsString = results.stream().map(Result::toString).collect(Collectors.joining(" "));
        String eResultsString = eResults.stream().map(eResult::toString).collect(Collectors.joining(" "));
        return "DataPipelineMessage{" +
                "pipelineId='" + pipelineId + '\'' +
                ", results=" + resultsString +
                ", eResults=" + eResultsString +
                '}';
    }
}
