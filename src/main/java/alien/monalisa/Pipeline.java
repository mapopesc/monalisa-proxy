package alien.monalisa;

import alien.monalisa.messages.SubscribeRequestMessage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Pipeline {
    final private String pipelineId;
    final private List<MonALISANode> producers;
    final private MonALISANode consumer;
    final private String farmSubscribePattern;
    final private String moduleSubscribePattern;
    final private Queue<List<Serializable>> dataQueue;

    public Pipeline(String pipelineId, MonALISANode consumer, String farmSubscribePattern, String moduleSubscribePattern) {
        this.pipelineId = pipelineId;
        this.farmSubscribePattern = farmSubscribePattern;
        this.moduleSubscribePattern = moduleSubscribePattern;
        this.producers = new ArrayList<>();
        this.consumer = consumer;
        this.dataQueue = new ConcurrentLinkedQueue<>();
    }

    public Pipeline(SubscribeRequestMessage subscribeRequestMessage, MonALISANode consumer) {
        this.pipelineId = subscribeRequestMessage.getPipelineId();
        this.farmSubscribePattern = subscribeRequestMessage.getFarmSubscribePattern();
        this.moduleSubscribePattern = subscribeRequestMessage.getModuleSubscribePattern();
        this.producers = new ArrayList<>();
        this.consumer = consumer;
        this.dataQueue = new ConcurrentLinkedQueue<>();
    }

    public String getPipelineId() {
        return pipelineId;
    }

    public List<MonALISANode> getProducers() {
        return producers;
    }

    public MonALISANode getConsumer() {
        return consumer;
    }

    public boolean addProducer(MonALISANode producer) {
        return producers.add(producer);
    }

    public boolean removeProducer(MonALISANode producer) {
        return producers.remove(producer);
    }

    public String getFarmSubscribePattern() {
        return farmSubscribePattern;
    }

    public String getModuleSubscribesPattern() {
        return moduleSubscribePattern;
    }
}
