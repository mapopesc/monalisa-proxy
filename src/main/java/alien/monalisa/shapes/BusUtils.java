package alien.monalisa.shapes;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class BusUtils {
    public static <T> Bus<T, T> getAndStartSingleThreadedBusFromOutput(OutputShape<T> outputShape, int queueSize, String busName) {
        final BusShape<T, T> startshape = new BusShape<>();
        final BusShape<T, T> finalShape = startshape.to(outputShape);

        return getAndStartSingleThreadedBusFromShape(finalShape, queueSize, busName);
    }

    public static <T> Bus<T, T> getAndStartSingleThreadedBusFromShape(BusShape<T, T> shape, int queueSize, String busName) {
        final BlockingQueue<T> queue = new LinkedBlockingQueue<>(queueSize);

        final Bus<T, T> result = new Bus<>(shape, queue, true, busName);
        final ExecutorService busThreadPool = Executors.newSingleThreadExecutor();
        busThreadPool.execute(result);

        return result;
    }
}
