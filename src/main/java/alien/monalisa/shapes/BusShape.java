package alien.monalisa.shapes;

import java.util.function.Function;
import java.util.logging.Logger;

public class BusShape<T, R> {
    public final Function<T, R> currentFunction;

    private final static Logger logger = Logger.getLogger(BusShape.class.getName());

    public BusShape() {
        this.currentFunction = null;
    }

    public BusShape(Function<T, R> currentFunction) {
        this.currentFunction = currentFunction;
    }

    public <V> BusShape<T, V> via(MapShape<R, V> mapFunction) {
        Function<R, V> newFunction = mapFunction::map;
        return chainShapes(newFunction);
    }

    public BusShape<T, R> via(FilterShape<R> filterFunction) {
        Function<R, R> newFunction = filterFunction::filter;
        return chainShapes(newFunction);
    }

    public BusShape<T, R> to(OutputShape<R> outputFunction) {
        Function<R, R> newFunction = outputFunction::write;
        return chainShapes(newFunction);
    }

    public R apply(T object) {
        assert currentFunction != null;
        return currentFunction.apply(object);
    }

    public void applyNoReturn(T object) {
        assert currentFunction != null;
        currentFunction.apply(object);
    }

    private <V> BusShape<T, V> chainShapes(Function<R, V> newFunction) {
        Function<T, V> result;
        if (currentFunction == null) {
            result = (Function<T, V>) newFunction;
        } else {
            result = currentFunction.andThen(newFunction);
        }
        return new BusShape<>(result);
    }
}
