package alien.monalisa.shapes;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.logging.Logger;

// TODO: + experiment with merging busshape and bus in a single class
public class Bus<T, R> implements Runnable{
	private final static Logger logger = Logger.getLogger(Bus.class.getName());
	private BusShape<T, R> shape;
	public BlockingQueue<T> busQueue;
	public long itemsOffered = 0;
	public long itemsProcessed = 0;
	public boolean activateStatistics = false;
	public String busName;

	public Bus (BusShape<T, R> shape, BlockingQueue<T> busQueue, boolean activateStatistics, String busName) {
		this(shape, busQueue);
		this.activateStatistics = activateStatistics;
		this.busName = busName;
	}

	public Bus (BusShape<T, R> shape, BlockingQueue<T> busQueue) {
		this.shape = shape;
		this.busQueue = busQueue;
	}

	public boolean offer(T object) {
		boolean res = busQueue.offer(object);
		if (res) {
			itemsOffered += 1;
		}
		return res;
	}

	public boolean offerAll (Collection<T> objects) {
		return objects.stream()
				.map(this::offer)
				.reduce(Boolean.TRUE, Boolean::logicalAnd);
	}

	@Override
	public void run(){
		final List<T> objs = new LinkedList<>();
		final Consumer<T> busConsumer = (t) -> {
			shape.applyNoReturn(t);
			itemsProcessed += 1;
		};

		if (activateStatistics) {
			Executors.newSingleThreadExecutor().execute(this::activatedStatisticsRun);
		}

		try {
			while (true) {
				objs.add(busQueue.take());
				busQueue.drainTo(objs);
				objs.forEach(busConsumer);
				objs.clear();
			}
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} finally {
			objs.forEach(busConsumer);
		}
	}

	public void activatedStatisticsRun() {
		long lastTime = System.currentTimeMillis();
		long lastOffered = this.itemsOffered;
		long lastProcessed = this.itemsProcessed;

		while(true) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			long currentTime = System.currentTimeMillis();
			long currentOffered = this.itemsOffered;
			long currentProcessed = this.itemsProcessed;
			double elapsedTimeInSeconds = (currentTime - lastTime) / 1000.0;
			double rateOffered = (currentOffered - lastOffered) / elapsedTimeInSeconds;
			double rateProcessed = (currentProcessed - lastProcessed) / elapsedTimeInSeconds;
			lastTime = currentTime;
			lastOffered = currentOffered;
			lastProcessed = currentProcessed;

			logger.info(busName + " Stats - Processed: " + currentProcessed + ", Offered: " + currentOffered + ", Processed Rate: " + String.format("%.2f", rateProcessed) + "/s, Offered Rate: " + String.format("%.2f", rateOffered) + "/s");
		}
	}
}
