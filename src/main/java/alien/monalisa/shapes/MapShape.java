package alien.monalisa.shapes;

import java.util.Collection;
import java.util.stream.Collectors;

@FunctionalInterface
public interface MapShape<I,O> {
    O map(I data);

    default Collection<O> map(Collection<I> c) {
        return c.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }
}
