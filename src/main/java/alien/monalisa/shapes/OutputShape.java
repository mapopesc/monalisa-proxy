package alien.monalisa.shapes;

import java.util.Collection;
import java.util.stream.Collectors;

@FunctionalInterface
public interface OutputShape<R> {
    R write(R data);

    default Collection<R> write(Collection<R> c) {
        return c.stream()
                .map(this::write)
                .collect(Collectors.toList());
    }
}
