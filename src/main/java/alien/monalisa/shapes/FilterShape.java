package alien.monalisa.shapes;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@FunctionalInterface
public interface FilterShape<T> {
	T filter (T data);

	default Collection<T> filter(Collection<T> c) {
		return c.stream()
				.map(this::filter)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}
}
