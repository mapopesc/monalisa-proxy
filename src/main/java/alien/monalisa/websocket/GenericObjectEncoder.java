package alien.monalisa.websocket;

import jakarta.websocket.EncodeException;
import jakarta.websocket.Encoder;
import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.FSTObjectOutput;

import java.io.IOException;
import java.io.OutputStream;

public class GenericObjectEncoder implements Encoder.BinaryStream<Object> {
    private static final FSTConfiguration FST = FSTConfiguration.createDefaultConfiguration();

    @Override
    public void encode(Object object, OutputStream os) throws EncodeException, IOException {
        final FSTObjectOutput output = FST.getObjectOutput(os);
        output.writeObject(object);
        output.flush();
        os.close();
    }
}
