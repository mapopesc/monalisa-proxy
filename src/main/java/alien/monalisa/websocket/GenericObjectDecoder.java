package alien.monalisa.websocket;

import jakarta.websocket.DecodeException;
import jakarta.websocket.Decoder;
import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.FSTObjectInput;

import java.io.IOException;
import java.io.InputStream;

public class GenericObjectDecoder implements Decoder.BinaryStream<Object> {
    private static final FSTConfiguration FST = FSTConfiguration.createDefaultConfiguration();

    @Override
    public Object decode(InputStream is) throws DecodeException, IOException {
        final FSTObjectInput input = FST.getObjectInput(is);
        Object res = null;
        try (is) {
            res = input.readObject();
        } catch (ClassNotFoundException e) {
            throw new DecodeException("<binary_data>", "could not decode because class not loaded",  e);
        }
        return res;
    }
}
