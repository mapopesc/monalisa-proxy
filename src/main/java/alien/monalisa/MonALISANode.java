package alien.monalisa;

import java.io.Serializable;
import java.util.Set;

public class MonALISANode implements Serializable {
    private static final long serialVersionUID = 5111582121757766242L;
    public final String serviceName;
    public final String ip;
    public final String hostname;
    public final Set<String> capabilities;

    public MonALISANode(String serviceName, String ip, String hostname, Set<String> capabilities) {
        this.serviceName = serviceName;
        this.ip = ip;
        this.hostname = hostname;
        this.capabilities = capabilities;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getIp() {
        return ip;
    }

    public String getHostname() {
        return hostname;
    }

    public Set<String> getCapabilities() {
        return capabilities;
    }
}
