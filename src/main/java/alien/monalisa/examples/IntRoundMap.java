package alien.monalisa.examples;

import alien.monalisa.shapes.MapShape;
import lia.Monitor.monitor.Result;

public class IntRoundMap implements MapShape<Result, Result> {
    @Override
    public Result map(Result data) {
        if (data == null) {
            return null;
        }

        return mapResult(data);
    }

    private Result mapResult(Result result) {
        for (int i = 0; i < result.param.length; i++) {
            result.param[i] = (int) Math.round(result.param[i]);
        }
        return result;
    }
}
