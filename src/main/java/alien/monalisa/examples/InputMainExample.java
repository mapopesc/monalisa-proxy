package alien.monalisa.examples;

import alien.monalisa.examples.output.LastSystemOutPrintlnOutput;
import alien.monalisa.examples.output.SystemOutPrintlnOutput;
import alien.monalisa.shapes.Bus;
import alien.monalisa.shapes.BusShape;
import lia.Monitor.modules.monProcStat;
import lia.Monitor.monitor.MNode;
import lia.Monitor.monitor.MonitoringModule;
import lia.Monitor.monitor.Result;

import java.net.InetAddress;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

// TODO: + testat injectare o gramada de mesaje si multiple scenarii de probleme
// TODO: + performance testing
public class InputMainExample {
	// policy -> pentru multi clienti inceti
	public static ExecutorService processingExecutor = Executors.newFixedThreadPool(2);
	
	private final static Logger logger = Logger.getLogger(InputMainExample.class.getName());
	public static Bus outputBus;
	
	private static Queue<Object> genModules (MonitoringModule mon) throws Exception {
		Random rand = new Random();
		int randNr = rand.nextInt(10);
		if (randNr == 0) {
			return null;
		}
		
		Queue<Object> r = new LinkedList<>();
		for (int i = 1; i <= randNr; i++) {
			Thread.sleep(1000L);
			Result res = (Result) mon.doProcess();
			if (res != null) {
				r.add(res);
			}
		}
		return r;
	}
	
	public static void main(String[] args) {
		
		final BusShape<Result, Result> newShape = new BusShape<Result, Result>()
//				.via(new IntRoundMap())
				.via(new ExampleFilter())
				.to(new SystemOutPrintlnOutput());
				// TODO: + add the ability to change busShape on the fly in the Bus
		
		int sizeQueue = 5;
		final BlockingQueue<Object> processingQueue = new LinkedBlockingQueue<>(sizeQueue);
		final Bus bus = new Bus(newShape, processingQueue);
		processingExecutor.submit(bus);
		
		
		final BlockingQueue<Object> outputQueueBus = new LinkedBlockingQueue<>(7);
		final BusShape<Result, Result> outputShape = (new BusShape<Result, Result>())
//				.via(new IntRoundMap())
				.to(new LastSystemOutPrintlnOutput());
		outputBus = new Bus(outputShape, outputQueueBus);
		processingExecutor.submit(outputBus);
		
		
		String host = "localhost";
		try {
			final monProcStat mon = new monProcStat();
			String address = null;
			
			try {
				address = InetAddress.getByName(host).getHostAddress();
			} catch (Exception var6) {
				System.out.println(" Can not get ip for node " + var6);
				System.exit(- 1);
			}
			
			mon.init(new MNode(host, address, null, null), null, null);
			try {
				while (true) {
					Queue<Object> res = genModules(mon);
					if (res != null && !res.isEmpty()) {
						bus.offerAll(res);
					}
					if (processingQueue.size() == 5) {
						logger.info("COADA DE PROCESARE ESTE PLINA");
					} else  {
						logger.info("DIMENSIUNE COADA DE PROCESARE: " + processingQueue.size());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
