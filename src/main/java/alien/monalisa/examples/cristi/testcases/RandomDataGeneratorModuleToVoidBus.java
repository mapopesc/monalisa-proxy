package alien.monalisa.examples.cristi.testcases;

import alien.monalisa.examples.cristi.RandomDataMonitoringModule;
import alien.monalisa.examples.cristi.VoidOutput;
import alien.monalisa.shapes.Bus;
import alien.monalisa.shapes.BusShape;
import lia.Monitor.monitor.MNode;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

public class RandomDataGeneratorModuleToVoidBus {
    private final static Logger logger = Logger.getLogger(RandomDataGeneratorModuleToVoidBus.class.getName());
    public final static int MON_THREAD_POOL_SIZE = 2;
    public final static ExecutorService monThreadPool = Executors.newFixedThreadPool(MON_THREAD_POOL_SIZE);
    public final static ExecutorService busThreadPool = Executors.newFixedThreadPool(1);
    public final static int BUS_QUEUE_SIZE = 1000;
    public final static int NR_ITER = 10;

    public static void prepareModule(Bus<Object, Object> bus) {
        final RandomDataMonitoringModule mon = new RandomDataMonitoringModule();
        mon.init(new MNode("localhost", "127.0.0.1", null, null), Integer.toString(NR_ITER));

        for (int i = 0; i < MON_THREAD_POOL_SIZE; ++i) {
            monThreadPool.execute(() -> {
                logger.info("Started thread that offers objects to bus");
                while (true) {
                    Object res;
                    try {
                        res = mon.doProcess();
                        if (res != null) {
                            bus.offer(res);
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }
    }

    public static void main(String[] args) throws Exception {
        logger.info("Starting RandomDataMonitoring module");
        final BusShape<Object, Object> moduleToVoidBus = new BusShape<>().to(new VoidOutput());

        final BlockingQueue<Object> outputQueueBus = new LinkedBlockingQueue<>(BUS_QUEUE_SIZE);

        final Bus<Object, Object> bus = new Bus<>(moduleToVoidBus, outputQueueBus);
        busThreadPool.execute(bus);
        prepareModule(bus);

        long lastTime = System.currentTimeMillis();
        long lastOffered = bus.itemsOffered;
        long lastProcessed = bus.itemsProcessed;

        while(true) {
            Thread.sleep(1000);
            long currentTime = System.currentTimeMillis();
            long currentOffered = bus.itemsOffered;
            long currentProcessed = bus.itemsProcessed;
            double rateOffered = ((double) (currentOffered - lastOffered)) / ((double) (currentTime - lastTime));
            double rateProcessed = ((double) (currentProcessed - lastProcessed)) / ((double) (currentTime - lastTime));
            lastTime = currentTime;
            lastOffered = currentOffered;
            lastProcessed = currentProcessed;
            logger.info("Bus offered " + currentOffered);
            logger.info("Bus processed " + currentProcessed);
            logger.info("Rate offered " + rateOffered);
            logger.info("Rate processed " + rateProcessed);
        }
    }
}
