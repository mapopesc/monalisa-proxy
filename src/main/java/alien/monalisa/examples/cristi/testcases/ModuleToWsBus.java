package alien.monalisa.examples.cristi.testcases;

import alien.monalisa.capabilities.farm.FarmCapability;
import alien.monalisa.examples.cristi.FarmWsOutput;
import alien.monalisa.shapes.Bus;
import alien.monalisa.shapes.BusShape;
import lia.Monitor.modules.monProcStat;
import lia.Monitor.monitor.MNode;

import java.net.InetAddress;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuleToWsBus {
    private final static Logger logger = Logger.getLogger(ModuleToWsBus.class.getName());
    public final static int MON_THREAD_POOL_SIZE = 2;
    public final static ExecutorService monThreadPool = Executors.newFixedThreadPool(MON_THREAD_POOL_SIZE);
    public final static ExecutorService busThreadPool = Executors.newFixedThreadPool(1);
    public final static int BUS_QUEUE_SIZE = 100;

    public static void prepareModule(Bus<Object, Object> bus) throws Exception {
        // test MonALISA module
        String host = "localhost";
        try {
            final monProcStat mon = new monProcStat();
            Logger monProcStatLogger = Logger.getLogger(monProcStat.class.getName());
            monProcStatLogger.setLevel(Level.OFF);

            String address = null;

            try {
                address = InetAddress.getByName(host).getHostAddress();
            } catch (Exception var6) {
                System.out.println(" Can not get ip for node " + var6);
                System.exit(-1);
            }

            mon.init(new MNode(host, address, null, null), null, null);

            for (int i = 0; i < MON_THREAD_POOL_SIZE; ++i) {
                monThreadPool.execute(() -> {
                    logger.info("Started thread that offers objects to bus");
                    while (true) {
                        Object res = null;
                        try {
                            res = mon.doProcess();
                            if (res != null) {
                                bus.offer(res);
                            }
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws Exception {
        FarmCapability farm = new FarmCapability(new Properties());
        farm.run();

        logger.info("Starting ModuleToVoidBus module");
        final BusShape<Object, Object> moduleToVoidBus = new BusShape<>()
                .to(new FarmWsOutput(farm.sender));

        final BlockingQueue<Object> outputQueueBus = new LinkedBlockingQueue<>(BUS_QUEUE_SIZE);

        final Bus<Object, Object> bus = new Bus<>(moduleToVoidBus, outputQueueBus);
        busThreadPool.execute(bus);
        prepareModule(bus);

        long lastTime = System.currentTimeMillis();
        long lastOffered = bus.itemsOffered;
        long lastProcessed = bus.itemsProcessed;

        while(true) {
            Thread.sleep(1000);
            long currentTime = System.currentTimeMillis();
            long currentOffered = bus.itemsOffered;
            long currentProcessed = bus.itemsProcessed;
            double rateOffered = ((double) (currentOffered - lastOffered)) / ((double) (currentTime - lastTime));
            double rateProcessed = ((double) (currentProcessed - lastProcessed)) / ((double) (currentTime - lastTime));
            lastTime = currentTime;
            lastOffered = currentOffered;
            lastProcessed = currentProcessed;
            logger.info("Bus offered " + currentOffered);
            logger.info("Bus processed " + currentProcessed);
            logger.info("Rate offered " + rateOffered);
            logger.info("Rate processed " + rateProcessed);
        }
    }
}
