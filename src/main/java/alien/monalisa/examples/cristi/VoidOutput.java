package alien.monalisa.examples.cristi;

import alien.monalisa.shapes.OutputShape;

import java.util.Collection;

public class VoidOutput implements OutputShape<Object> {
    @Override
    public Object write(Object data) {
        return data;
    }

    @Override
    public Collection<Object> write(Collection<Object> c) {
        return c;
    }
}
