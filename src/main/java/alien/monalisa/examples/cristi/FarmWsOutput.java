package alien.monalisa.examples.cristi;

import alien.monalisa.capabilities.farm.FarmWsClient;
import alien.monalisa.shapes.OutputShape;

import java.util.Collection;

public class FarmWsOutput implements OutputShape<Object> {
    private FarmWsClient client;
    public FarmWsOutput(FarmWsClient client) {
        this.client = client;
    }

    @Override
    public Object write(Object data) {
        client.sendMessage(data);
        return data;
    }

    @Override
    public Collection<Object> write(Collection<Object> c) {
        return OutputShape.super.write(c);
    }
}
