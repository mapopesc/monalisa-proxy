package alien.monalisa.examples;

import alien.monalisa.shapes.FilterShape;
import lia.Monitor.monitor.Result;

public class ExampleFilter implements FilterShape<Result> {
    
    @Override
    public Result filter(Result data) {
        if (data == null) {
            return null;
        }

        return filterResult(data);
    }
    
    private Result filterResult(Result result) {
        double[] params = result.param;
        if (params.length > 0 && params[0] < 40) {
            return result;
        }
        
        return null;
    }
}
