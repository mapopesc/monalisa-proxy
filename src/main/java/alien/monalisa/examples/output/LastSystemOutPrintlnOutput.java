package alien.monalisa.examples.output;


import alien.monalisa.shapes.OutputShape;
import lia.Monitor.monitor.Result;

import java.util.Collection;
import java.util.logging.Logger;

public class LastSystemOutPrintlnOutput implements OutputShape<Result> {
	private final static Logger logger = Logger.getLogger(LastSystemOutPrintlnOutput.class.getName());
	@Override
	public Result write (Result data) {
		logger.info("THREAD OUTPUT " + Thread.currentThread().getId());
		if (data == null) {
			return null;
		}
		
		logger.info("element: " + data);
		
		try {
//			Thread.sleep(1000L);
			Thread.sleep(100000L);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}

        logger.info("NOILE VALORI: " + data);

        return null;
	}
	
	public void writeCollection(Collection<Result> data) {
		logger.info("***********************************************************************");
		
		for (Result o : data) {
			logger.info("NOILE VALORI: " + o);
		}
		data.clear();
  
		
		logger.info("***********************************************************************");
	}
	
}
