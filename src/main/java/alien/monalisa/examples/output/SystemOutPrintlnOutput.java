package alien.monalisa.examples.output;

import alien.monalisa.examples.InputMainExample;
import alien.monalisa.shapes.OutputShape;
import lia.Monitor.monitor.Result;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;


public class SystemOutPrintlnOutput implements OutputShape<Result> {
    private final static Logger logger = Logger.getLogger(SystemOutPrintlnOutput.class.getName());
    static BlockingQueue<Object> outputProcessingQueue = new LinkedBlockingQueue<>(10);
    
    public Result write(Result data) {
        if (data == null) {
            return null;
        }
        if(data instanceof Collection<?> && ! ((Collection<?>) data).isEmpty()) {
            writeCollection((Collection<Result>) data);
        } else if (data instanceof Result) {
            writeResult((Result) data);
        }
        
//        while(!outputProcessingQueue.isEmpty()) {
//            logger.info("ELEMENT: " + outputProcessingQueue.poll());
//        }

        InputMainExample.outputBus.offerAll(outputProcessingQueue);
        outputProcessingQueue.clear();
	    return null;
    }
    
    public void writeCollection(Collection<Result> data) {
	    for (Object o : data) {
            if (!outputProcessingQueue.offer(o)) {
                break;
            }
	    }
     
//	    for (Result o : data) {
////            logger.info("care thread: " + Thread.currentThread().getId());
//		    logger.info("ELEMENT: " + o);
//	    }
//        logger.info("--------------------------------------------------");
    }

    public void writeResult(Result result) {
        try {
            outputProcessingQueue.put(result);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
//        logger.info("ELEMENT: " + result);
    }
}
