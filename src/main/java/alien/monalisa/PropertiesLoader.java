package alien.monalisa;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    private final static String MAIN_PROPERTIES_FILE_PATH = "application.properties";
    private static Properties mainPropertiesFile;

    public static Properties getMainPropertyFile() {
        synchronized (PropertiesLoader.class) {
            if (mainPropertiesFile == null) {
                mainPropertiesFile = readPropertyFile(MAIN_PROPERTIES_FILE_PATH);
            }
            return new Properties(mainPropertiesFile);
        }
    }

    public static Properties readPropertyFile(String propertyFilePath) {
        try (InputStream input = new FileInputStream(propertyFilePath)) {
            final Properties prop = new Properties();
            prop.load(input);

            return prop;
        } catch (IOException exception) {
            throw new IllegalStateException("Could not load properties file located at " + propertyFilePath, exception);
        }
    }
}
