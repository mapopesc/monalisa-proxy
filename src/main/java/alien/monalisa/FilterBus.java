package alien.monalisa;

import alien.monalisa.capabilities.farm.FarmCapability;
import alien.monalisa.shapes.Bus;
import alien.monalisa.shapes.OutputShape;
import lia.Monitor.Filters.GenericMLFilter;
import lia.Monitor.monitor.Result;
import lia.Monitor.monitor.monPredicate;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

import static alien.monalisa.shapes.BusUtils.getAndStartSingleThreadedBusFromOutput;

public class FilterBus extends GenericMLFilter {
    private final static Logger logger = Logger.getLogger(FilterBus.class.getName());
    private final Bus<Result, Result> internalBus;
    private static List<Bus<Result, Result>> registeredBuses = new CopyOnWriteArrayList<>();

    public FilterBus(String farmName) {
        super(farmName);
        // farm config file setup
        String farmConfigFile = System.getProperty("FARM_CONFIG_FILE");
        if (farmConfigFile == null) {
            farmConfigFile = "/home/mark/git/gitlab.cern.ch/mapopesc/monalisa-proxy/src/main/resources/default-farm.properties";
        }

        // farm setup
        final Properties testingFarmProperties = new Properties();
        try {
            testingFarmProperties.load(new FileInputStream(farmConfigFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        final FarmCapability farm = new FarmCapability(testingFarmProperties);
        farm.run();

        internalBus = getAndStartSingleThreadedBusFromOutput(new MultipleBusesOutput(), 100, "filterBus");
    }
    @Override
    public String getName(){
        return "FilterBus";
    }

    @Override
    public long getSleepTime () {
        return 60000;
    }

    @Override
    public monPredicate[] getFilterPred () {
        return null;
    }

    @Override
    public void notifyResult (Object o) {
        addNewResult(o);
    }

    public Object expressResults () {
        return null;
    }

    public void addNewResult (Object o) {
        if (o == null) {
            return;
        }

        if (o instanceof Collection) {
            internalBus.offerAll((Collection<Result>) o);
        } else if (o instanceof Result){
            internalBus.offer((Result) o);
        }
    }

    public static void addNewBus (Bus<Result, Result> newBus) {
        registeredBuses.add(newBus);
    }

    static class MultipleBusesOutput implements OutputShape<Result> {
        @Override
        public Result write(Result data) {
            for (Bus<Result, Result> bus : registeredBuses) {
                bus.offer(data);
            }
            return data;
        }
    }
}
